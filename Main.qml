import QtQuick 2.0
import SddmComponents 2.0

Rectangle {
  id: container
  width: 1024
  height: 768

  LayoutMirroring.enabled: Qt.locale().textDirection == Qt.RightToLeft
  LayoutMirroring.childrenInherit: true

  property int sessionIndex: session.index

  TextConstants {
    id: textConstants
  }

  Connections {
    target: sddm
    onLoginSucceeded: {
    }

    onLoginFailed: {
      errorMessage.color = "#ff6c6b"
      errorMessage.text = textConstants.loginFailed
    }
  }

  FontLoader {
    id: textFont; name: config.displayFont
  }

  Repeater {
    model: screenModel
    Background {
      x: geometry.x; y: geometry.y; width: geometry.width; height:geometry.height
      source: config.background
      fillMode: Image.PreserveAspectCrop
      onStatusChanged: {
        if (status == Image.Error && source != config.defaultBackground) {
          source = config.defaultBackground
        }
      }
    }
  }
  
  // Main login form container
  Rectangle {
    id: mainContainer;
    width: 700;
    height: parent.height;
    color: "#23262F";
    x: parent.width - 700;
    
    Column {
      width: parent.width;
      height: parent.height;
      
      // Clock
      Rectangle {
        id: clockContainer;
        width: parent.width;
        height: parent.height / 3;
        color: "#23262F";

        Clock {
          id: clock;
          anchors.centerIn: parent;
          color: "#bbc2cf";
          anchors.horizontalCenter: parent.horizontalCenter;
        }
      }
      
      Rectangle {
        width: parent.width;
        height: parent.height / 2;
        color: "#23262F";

        Item {
          width: 300;
          height: 400;
          anchors.horizontalCenter: parent.horizontalCenter;
          anchors.verticalCenter: parent.verticalCenter;
          
          Column {
            width: parent.width;
            spacing: 10;
            anchors.margins: 10;

            // Username
            Text {
              id: username_label;
              width: parent.width;
              text: "Username";
              font.family: textFont.name;
              font.bold: true;
              font.pixelSize: 18;
              color: "#bbc2cf";
            }
            
            TextBox {
              id: username;
              width: parent.width;
              height: 40;
              radius: 2;
              text: userModel.lastUser;
              font.pixelSize: 18;
              color: "#2a2d38";
              textColor: "#bbc2cf"
              borderColor: "#2a2d38";
              
              Keys.onPressed: {
                if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter) {
                  sddm.login(username.text, password.text, session.index)
                  event.accepted = true
                }
              }

            }

            Text {
              id: password_label;
              width: parent.width;
              text: textConstants.password;
              font.family: textFont.name;
              font.bold: true;
              font.pixelSize: 18;
              color: "#bbc2cf";
            }
            
            PasswordBox {
              id: password;
              width: parent.width;
              height: 40;
              radius: 2;
              font.pixelSize: 15;
              color: "#2a2d38";
              textColor: "#bbc2cf"
              borderColor: "#2a2d38";

              Keys.onPressed: {
                if (event.key === Qt.Key_Return || event.key === Qt.Key_Enter) {
                  sddm.login(username.text, password.text, session.index)
                  event.accepted = true
                }
              }

            }
            
            Row {
              width: parent.width;
              height: 30;

              Text {
                id: errorMessage;
                anchors.left: name.left;
                text: "";
                font.family: textFont.name;
                font.pixelSize: 13;
                color: "red";
              }

              Button {
                id: login_button;
                text: textConstants.login;
                width: 80;
                height: 30;
                radius: 2;
                font.pixelSize: 14;
                color: "#5294E2";
                textColor: "#ffffff";
                anchors.right: parent.right;
                onClicked: sddm.login(username.text, password.text, session.index)
              }
            }
          }
        }
      }
      
      Rectangle {
        height: parent.height / 3;
        width: parent.width;
        color: "#23262F";
            
        Row {
          spacing: 30;
          anchors.horizontalCenter: parent.horizontalCenter;
          anchors.bottom: parent.botton;

          ImageButton {
            id: reboot_button;
            source: "svg/reboot.svg";
            //visible: sddm.canReboot;
            onClicked: sddm.reboot();
          }

          ImageButton {
            id: shutdown_button;
            source: "svg/power.svg";
            //visible: sddm.canPowerOff;
            onClicked: sddm.powerOff();
          }
          
          ImageButton {
            id: suspend_button;
            source: "svg/suspend.svg";
            //visible: sddm.canSuspend;
            onClicked: sddm.suspend();

          }
        }
      }
    }
  }

  Rectangle {
    color: "transparent";
    x: 10;
    y: 10;
    height: 40;
    width: 180;

    Row {
      anchors.left: parent.left;
      height: parent.height;
      spacing: 10;

      ComboBox {
        id: session;
        width: 100;
        height: 25;
        color: "#23262F";
        textColor: "#bbc2cf";
        borderColor: "transparent";
        arrowIcon: "svg/arrow-down.svg";
        arrowColor: "#23262F";
        hoverColor: "transparent";

        model: sessionModel;
        index: sessionModel.lastIndex;
        font.family: textFont.name;
        font.pixelSize: 14;
      }

      ComboBox {
        id: layout;
        
        model: keyboard.layouts
        index: keyboard.currentLayout
        width: 70;
        height: 25;
        color: "#23262F";
        textColor: "#bbc2cf";
        borderColor: "transparent";
        arrowIcon: "svg/arrow-down.svg";
        arrowColor: "#23262F";
        hoverColor: "transparent";
        onValueChanged: keyboard.currentLayout = id;

        Connections {
          target: keyboard;
          onCurrentLayoutChange: combo.index = keyboard.currentLayout;
        }

         rowDelegate: Rectangle {
          color: "#23262F";

          Text {
            anchors.margins: 4
            anchors.top: parent.top
            anchors.bottom: parent.bottom


            text: modelItem ? modelItem.modelData.shortName : "zz"
            font.family: textFont.name
            font.pixelSize: 14
            color: "#bbc2cf"
          }
        }
      }
    }
  }
}

